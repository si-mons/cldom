package gateways;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import crypto.Ticker;
import crypto.Trade;

public class HitBtcGateway implements AbstractPortalGateway{

	private String symbol;

	private static String apiUri = "https://api.hitbtc.com/api/1/";

	private static String serviceTradingBalance = "trading/balance";
	private static String servicePublicTicker = "public/%s/ticker";
	private static String servicePublicTrades = "public/%s/trades?start_index=0&by=ts"
			+ "&format_item=object&format_price=number&format_amount=number&from=%s&till=%s&max_results=%s&side=%s";
	private static String serviceTradingNewOrder = "trading/new_order?apikey=%s&nonce=%s&clientOrderId=%s&symbol=%s&side=%s"
			+ "&quantity=%s&type=limit&price=%s&timeInForce=%s";

	private static String publicKey = "9a74b737a02fed26695fb6d223bf4689";
	private static String privateKey = "36cebc916149a42404d0f97f69596f7d";

	private PoolingHttpClientConnectionManager connManager;
	private CloseableHttpClient client;

	private static Type twoDimType = new TypeToken<Map<String, List<Map<String, String>>>>() {}.getType();

	private class TradeResponse {

		private LinkedHashSet<Trade> trades;

		public LinkedHashSet<Trade> getTrades() {
			return trades;
		}
	}

	private class TradeLoaderThread extends Thread{

		private String symbol;
		private long from;
		private long till;
		private int maxResults;
		private boolean withSide;
		private boolean finished;

		private LinkedHashSet<Trade> trades;

		public TradeLoaderThread(String symbol, long from, long till, int maxResults, boolean withSide) {
			this.symbol = symbol;
			this.from = from;
			this.till = till;
			this.maxResults = maxResults;
			this.finished= false;
			this.trades = new LinkedHashSet<>();
			this.withSide = withSide;
		}

		public boolean isFinished() {
			return finished;
		}

		@Override
		public void run() {
			this.trades = getTrades(symbol, from, till, maxResults, withSide);
		}

		public LinkedHashSet<Trade> getTrades() {
			if (this.isAlive()) {
				throw new IllegalAccessError("thread still alive");
			}

			return this.trades;
		}

		private LinkedHashSet<Trade> getTrades(String symbol, long from, long till, int maxResults, boolean withSide) {
			// build message
			String message = String.format(servicePublicTrades, symbol, from, till, maxResults, withSide);
			// build request
			HttpGet httpGet = new HttpGet(apiUri + message);

			try {
				// do request
				CloseableHttpResponse response = client.execute(httpGet);

				// check request
				if (response.getStatusLine().getStatusCode() == 200) {

					// create reader
					InputStreamReader responseReader = new InputStreamReader(response.getEntity().getContent());
					// create map from json data
					TradeResponse tradeResp = new Gson().fromJson(responseReader, TradeResponse.class);

					// close response
					response.close();

					// execute trade fetching recursive to get all trades
					if (tradeResp.getTrades().size() == 1000) {
						// use the time of the last trade to continue
						Trade lastTrade = null;
						for (Trade trade : tradeResp.getTrades()) {
							lastTrade= trade;
						}

						LinkedHashSet<Trade> innerTrades = getTrades(symbol, lastTrade.getDate() - 1, till,
								maxResults, withSide);

						// add remaining trades
						innerTrades.addAll(tradeResp.getTrades());

						// return trade list incl remaining trades
						return innerTrades;

					} else {
						// set finished flag
						this.finished = true;
						// return trade list
						return tradeResp.getTrades();
					}
				} else {
					// execute again
					return getTrades(symbol, from, till, maxResults, withSide);
				}

			} catch (Exception e) {
				e.printStackTrace();

				return new LinkedHashSet<Trade>();
			} finally {
				httpGet.releaseConnection();
			}
		}
	}

	public enum TimeInForce {
		GoodTilCanceled("GTC"),
		ImmediateOrCancel("IOC"),
		FillOrKill("FOK");

		private final String abbrev;

		@Override
		public String toString() {
			return abbrev;
		}
		TimeInForce(String abbrev){
			this.abbrev = abbrev;
		}
	}

	public HitBtcGateway(String symbol) {
		this.symbol = symbol;
		this.connManager = new PoolingHttpClientConnectionManager();
		int maxConns = 40;
		connManager.setMaxTotal(maxConns);
		connManager.setDefaultMaxPerRoute(maxConns);
		HttpHost host = new HttpHost(apiUri, 443);
		connManager.setMaxPerRoute(new HttpRoute(host), maxConns);
		this.client = HttpClients.custom().setConnectionManager(connManager).build();
	}

	public static String hmacDigest(String message, String secretKey) {
		String digest = null;
		String algo = "HmacSHA512";
		try {
			// hash message using secret key
			SecretKeySpec key = new SecretKeySpec((secretKey).getBytes("UTF-8"), algo);
			Mac mac = Mac.getInstance(algo);
			mac.init(key);
			byte[] bytes = mac.doFinal(message.getBytes("UTF-8"));
			StringBuilder hash = new StringBuilder();

			for (int i = 0; i < bytes.length; i++) {
				String hex = Integer.toHexString(0xFF & bytes[i]);
				if (hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);

			}
			digest = hash.toString();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}

		return digest;
	}

	public List<Map<String, String>> getTradingBalance() {
		// create date obj for nonce param
		Date date = new Date();
		// create message to send
		String message = serviceTradingBalance + "?nonce=" + date.getTime() + "&apikey=" + publicKey;

		try {
			// create http get and hash message using secret
			HttpGet httpGet = new HttpGet(apiUri + message);
			httpGet.addHeader("X-Signature", hmacDigest(message, privateKey));

			// execute request
			CloseableHttpResponse response = client.execute(httpGet);

			// check response
			if (response.getStatusLine().getStatusCode() == 200) {
				// create reader
				InputStreamReader responseReader = new InputStreamReader(response.getEntity().getContent());
				// create map from json data
				Map<String, List<Map<String, String>>> data = new Gson().fromJson(responseReader, twoDimType);

				// close response
				response.close();

				// return data map
				return data.get("balance");

			} else {
				// close response
				response.close();

				return null;
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Ticker getTicker() {
		// build message
		String message = String.format(servicePublicTicker, symbol);

		try {
			// build request
			HttpGet httpGet = new HttpGet(apiUri + message);

			// do request
			CloseableHttpResponse response = client.execute(httpGet);

			// check request
			if (response.getStatusLine().getStatusCode() == 200) {

				// create reader
				InputStreamReader responseReader = new InputStreamReader(response.getEntity().getContent());
				// create map from json data
				Ticker ticker = new Gson().fromJson(responseReader, Ticker.class);

				// close response
				response.close();

				// return data map
				return ticker;

			} else {
				// close response
				response.close();

				return null;
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Ticker getTicker(long timestamp) {
		return null;
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till) {
		return getPublicTrades(from, till, 1000, false);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults) {
		return getPublicTrades(from, till, maxResults, false);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, boolean withSide) {
		return getPublicTrades(from, till, 1000, withSide);
	}

	/**
	 * Fetch past trades to a specific symbol.
	 * @param symbol The Symbol to get trades to ( f.e. "ETHBTC")
	 * @param timestamp The timestamp to use to start calculating in wich time window to get trades
	 * @param intervallInMinutes How long the intervall to get the trades from should be.
	 * 			F.e. if provided 60, all returned trades happened within 60 minutes.
	 * @param index The index indicating how far into the past should be looked.
	 * 			F.e. if provided 3 (and intervallInMinutes=60) then
	 * 			all trades that happened in the hour that was 3 * 60 mins ago are returned.
	 * @return A list of all trade objects that were found
	 */
	// TODO update java doc
	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults, boolean withSide) {
		// create a thread for every day (but not gt 30/lt 3 threads total)
		int oneDay = 86400000;
		// make threads equal in size
		int threadCount = (int)((till-from) / oneDay);
		threadCount = threadCount > 35 ? 35 : threadCount < 3 ? 3 : threadCount;

		// get equal time range to use for every thread based on thread count
		long timeRangeEach = (till-from) / threadCount;
		
		// create threads
		ArrayList<TradeLoaderThread> threads = new ArrayList<TradeLoaderThread>(threadCount);
		for (int i = 0; i < threadCount; i++) {
			// calculate from and till of thread
			long thrFrom = from + (timeRangeEach * i);
			long thrTill = (i == threadCount-1) ? till : thrFrom + timeRangeEach - 1;
			TradeLoaderThread thread = new TradeLoaderThread(symbol, thrFrom, thrTill, maxResults, withSide);
			threads.add(thread);
			thread.start();
		}

		// result set
		LinkedHashSet<Trade> trades = new LinkedHashSet<Trade>();

		// join threads
		for (TradeLoaderThread thread : threads) {
			try {
				// wait 10 mins max per thread
				thread.join(60000 * 10);
				// check if thread was finished correctly
				if (!thread.isFinished()) {
					throw new RuntimeException("there was a connection error as not all trade loader threads could finish with success.");
				}
				// put all trades together
				trades.addAll(thread.getTrades());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// return all trades
		return trades;
	}

	public void createNewOrder (String symbol, String side, int qty, BigDecimal price, TimeInForce timeInForce) {
		// create date obj for nonce param
		Date date = new Date();
		// create message to send
		String message = String.format(serviceTradingNewOrder, publicKey, date.getTime(), date.getTime(),
				symbol, side, qty, price, timeInForce);

		try {
			// create http get and hash message using secret
			HttpGet httpGet = new HttpGet(apiUri + message);
			httpGet.addHeader("X-Signature", hmacDigest(message, privateKey));

			// execute request
			CloseableHttpResponse response = client.execute(httpGet);

			// check response
			if (response.getStatusLine().getStatusCode() == 200) {
				// create reader
				InputStreamReader responseReader = new InputStreamReader(response.getEntity().getContent());
				// create map from json data
				Map<String, List<Map<String, String>>> data = new Gson().fromJson(responseReader, twoDimType);

				// close response
				response.close();

				// return data map
				// TODO


			} else {
				// close response
				response.close();

//				return null;
			}

		} catch (IOException e) {
			e.printStackTrace();
//			return null;
		}
	}
}
