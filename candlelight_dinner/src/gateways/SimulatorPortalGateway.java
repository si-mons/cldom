package gateways;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import util.DBUtil;
import util.DBConnector;
import crypto.Ticker;
import crypto.Trade;

public class SimulatorPortalGateway implements AbstractPortalGateway{

	private String symbol;

	private DBConnector connector;

	public SimulatorPortalGateway(String symbol) {
		this.symbol = symbol;
		this.connector = new DBConnector();
	}

	@Override
	public List<Map<String, String>> getTradingBalance() {
		return null;
	}

	@Override
	public Ticker getTicker() {
		return null;
	}

	@Override
	public Ticker getTicker(long timestamp) {
		// get next trade where side = sell
		Trade nextSell = DBUtil.loadTrade(symbol, timestamp, "sell", connector);
		// get next trade where side = buy
		Trade nextBuy = DBUtil.loadTrade(symbol, timestamp, "buy", connector);

		// use higher price for ask
		Ticker t = null;
		if (nextSell.getPrice().compareTo(nextBuy.getPrice()) > 0) {
			t = new Ticker(symbol, nextBuy.getPrice(), nextSell.getPrice());
		} else {
			t = new Ticker(symbol, nextSell.getPrice(), nextBuy.getPrice());
		}
		// return created fake ticker
		return t;
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till) {
		return getPublicTrades(from, till, -1, false);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults) {
		return getPublicTrades(from, till, maxResults, false);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, boolean withSide) {
		return getPublicTrades(from, till, -1, withSide);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults, boolean withSide) {
		// return trades from DB
		LinkedHashSet<Trade> resultSet = DBUtil.loadTrades(symbol, from, till, connector);

		// check for max results counter
		if (maxResults > 0 && resultSet.size() > maxResults) {
			// only return proper amount of trades
			LinkedHashSet<Trade> restrictedResultSet = new LinkedHashSet<Trade>();
			for (Trade trade : resultSet) {
				restrictedResultSet.add(trade);
				// check size
				if (restrictedResultSet.size() == maxResults) {
					break;
				}
			}

			// return restricted result set
			return restrictedResultSet;
		} else {

			// return result set
			return resultSet;
		}
	}
}
