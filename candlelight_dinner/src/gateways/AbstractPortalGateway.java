package gateways;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import crypto.Ticker;
import crypto.Trade;

public interface AbstractPortalGateway {
	public List<Map<String, String>> getTradingBalance();
	public Ticker getTicker();
	public Ticker getTicker(long timestamp);
	public LinkedHashSet<Trade> getPublicTrades(long from, long till);
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults);
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, boolean withSide);
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults, boolean withSide);
}
