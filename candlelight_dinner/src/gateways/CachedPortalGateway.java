package gateways;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import crypto.Ticker;
import crypto.Trade;

public class CachedPortalGateway implements AbstractPortalGateway {

	private AbstractPortalGateway portalGateway;
	private TreeSet<Trade> trades;

	boolean inUse;
	long from;
	long till;

	public CachedPortalGateway(AbstractPortalGateway portalGateway) {
		this.inUse = false;
		this.from = -1;
		this.till = -1;
		this.trades = new TreeSet<>();

		if (portalGateway.getClass().equals(this.getClass())) {
			throw new RuntimeException("portal gateway instance may not be a cached portal gateway");
		}
		this.portalGateway = portalGateway;
	}

	public SortedSet<Trade> getTrades() {
		return trades;
	}

	@Override
	public List<Map<String, String>> getTradingBalance() {
		return portalGateway.getTradingBalance();
	}

	@Override
	public Ticker getTicker() {
		return portalGateway.getTicker();
	}

	@Override
	public Ticker getTicker(long timestamp) {
		return portalGateway.getTicker(timestamp);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till) {
		return getPublicTrades(from, till, -1, false);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults) {
		return getPublicTrades(from, till, maxResults, false);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, boolean withSide) {
		return getPublicTrades(from, till, -1, withSide);
	}

	@Override
	public LinkedHashSet<Trade> getPublicTrades(long from, long till, int maxResults, boolean withSide) {
		// result set
		LinkedHashSet<Trade> resultSet;

		// check if requested trades are already cached
		if (!inUse) {

			// update from and till as well as in use flag
			this.from = from;
			this.till = till;
			this.inUse = true;

			// read new trades
			resultSet = portalGateway.getPublicTrades(from, till);

			// add new trades to sorted set
			trades.addAll(resultSet);

		} else {
			// check if all trades are cached
			if (this.from <= from && this.till >= till) {

				// get sub set of cached trades
				resultSet = new LinkedHashSet<Trade>(trades.subSet(new Trade(from), new Trade(till)));

			} else {
//System.out.println("using db "+trades.size());
				// check what data is needed
				if (this.from > from) {
					// add missing trades
					trades.addAll(portalGateway.getPublicTrades(from, this.from));

					// update from
					this.from = from;
				}

				if (this.till < till) {
					// add missing trades
					trades.addAll(portalGateway.getPublicTrades(this.till, till));

					// update till
					this.till = till;
				}

				// get sub set of cached trades
				resultSet = new LinkedHashSet<Trade>(trades.subSet(new Trade(from), new Trade(till)));
			}
		}

		// check for max results counter
		if (maxResults > 0 && resultSet.size() > maxResults) {
			// only return proper amount of trades
			LinkedHashSet<Trade> restrictedResultSet = new LinkedHashSet<Trade>();
			for (Trade trade : resultSet) {
				restrictedResultSet.add(trade);
				// check size
				if (restrictedResultSet.size() == maxResults) {
					break;
				}
			}

			// return restricted result set
			return restrictedResultSet;
		} else {

			// return result set
			return resultSet;
		}
	}
}
