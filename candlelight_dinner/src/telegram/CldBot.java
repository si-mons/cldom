package telegram;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import gateways.HitBtcGateway;

public class CldBot extends TelegramLongPollingBot {

	private static final String botUsername = "candlelightdinner";
	private static final String botToken = "456300149:AAHh00hRARLwFktKWo39HC5sJUsZokkqp4Q";

	public CldBot() {
	}

	@Override
	public String getBotUsername() {
		return botUsername;
	}

	@Override
	public void onUpdateReceived(Update update) {
		// We check if the update has a message and the message has text
		if (update.hasMessage() && update.getMessage().hasText()) {
			System.out.println(update.getMessage().getText());
			String firstName = update.getMessage().getFrom().getFirstName();
			HitBtcGateway gw = new HitBtcGateway("BTCUSD");

			String strMessage = String.format("Whats kickin' %s - Im the god bot of trading if you will - "
					+ "I cannot do much yet but my father Simon is doing his best to make himself rich "
					+ "and me famous! Btw Bitcoin is currently at %s$", firstName, gw.getTicker().getAsk());
			
			SendMessage message = new SendMessage()
					.setChatId(update.getMessage().getChatId())
					.setText(strMessage);

			try {
				execute(message); // Call method to send the message
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getBotToken() {
		return botToken;
	}

}
