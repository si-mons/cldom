package main;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import gateways.CachedPortalGateway;
import gateways.SimulatorPortalGateway;
import statistic.Simulation;
import telegram.CldBot;

public class Application {

	public static void main(String[] args) {
//		ApiContextInitializer.init();
//
//		TelegramBotsApi botsApi = new TelegramBotsApi();
//
//		try {
//			botsApi.registerBot(new CldBot());
//		} catch (TelegramApiException e) {
//			e.printStackTrace();
//		}
//		ImportUtil.importOneYearTestTrades(new DBConnector());

		String symbolAlt = "LTC";
		String symbolMain = "BTC";

		CachedPortalGateway cg = new CachedPortalGateway(new SimulatorPortalGateway(symbolAlt+symbolMain));
		long start = new GregorianCalendar(2017, 5, 1).getTimeInMillis();
		long end = new GregorianCalendar(2017, 6, 1).getTimeInMillis();

		Simulation s = new Simulation(start, end, cg);
		s.simulate(symbolAlt, symbolMain, new BigDecimal("0.1"));

		
	}
}