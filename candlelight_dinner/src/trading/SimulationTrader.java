package trading;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.LinkedHashSet;

import crypto.Trade;
import gateways.AbstractPortalGateway;
import statistic.AnalizeSettings;
import statistic.AnalizeSettings.IntSetting;
import statistic.MarketAnalyzer;

public class SimulationTrader extends Trader implements Comparable<SimulationTrader>{

	private BigDecimal cashAlt;
	private BigDecimal cashMain;

	private long currentTime;
	private long endTime;
	private boolean finished;
	private SimulationStatistic stats;

	public class SimulationStatistic implements Comparable<SimulationStatistic>{

		private LinkedHashSet<Trade> trades;
		private Trade lastBuyTrade;

		private int countProfitSells;
		private int countEvenSells;
		private int countLosingSells;

		private BigDecimal performancePercentage;

		public SimulationStatistic() {
			this.trades = new LinkedHashSet<Trade>();

			this.countProfitSells = 0;
			this.countEvenSells = 0;
			this.countLosingSells = 0;

			this.performancePercentage = new BigDecimal(1);
		}

		public int getCountProfitSells() {
			return countProfitSells;
		}

		public int getCountEvenSells() {
			return countEvenSells;
		}

		public int getCountLosingSells() {
			return countLosingSells;
		}

		public BigDecimal getPerformancePercentage() {
			return performancePercentage;
		}

		public LinkedHashSet<Trade> getTrades() {
			return trades;
		}

		public boolean addTrade(Trade trade) {
			// check stats
			if (trade.getSide().equals("buy")) {
				this.lastBuyTrade = trade;
			} else {
				// check profit/loss
				int buyComparedToSell = this.lastBuyTrade.getPrice().compareTo(trade.getPrice());
				if (buyComparedToSell < 0) {
					countProfitSells++;
				} else if (buyComparedToSell > 0) {
					countLosingSells++;
				} else {
					countEvenSells++;
				}
				// update performance
				this.performancePercentage = performancePercentage
						.divide(this.lastBuyTrade.getPrice(), MathContext.DECIMAL32)
						.multiply(trade.getPrice(), MathContext.DECIMAL32);
			}

			// add trade
			return this.trades.add(trade);
		}

		public int getTradeCount() {
			return this.trades.size();
		}

		@Override
		public int compareTo(SimulationStatistic s) {
			// first compare performace
			int performanceCompare = this.getPerformancePercentage()
					.compareTo(s.getPerformancePercentage());
			if (performanceCompare == 0) {
				// statistics with equal performance are considered
				// better if less trades happened due to saved trading fees
				return this.getTradeCount() < s.getTradeCount() ? 1 :
					this.getTradeCount() > s.getTradeCount() ? -1 : 0;
			} else {
				return performanceCompare;
			}
		}

		@Override
		public String toString() {
			return String.format("[TRADING-STAT: %spct - %s+ %s= %s-]", this.getPerformancePercentage(),
					this.getCountProfitSells(), this.getCountEvenSells(), this.getCountLosingSells());
		}
	}

	public SimulationTrader(String symbolAlt, String symbolMain, String pseudonym, AnalizeSettings analizeSettings, long intervallInMinutes, AbstractPortalGateway portalGateway,
			BigDecimal initialCashMain, long startTime, long endTime) {
		super(symbolAlt, symbolMain, pseudonym, analizeSettings, intervallInMinutes, portalGateway);

		this.cashMain = initialCashMain;
		this.cashAlt = new BigDecimal(0);
		this.currentTime = startTime;
		this.endTime = endTime;
		this.finished = false;

		this.stats = new SimulationStatistic();
	}

	@Override
	public BigDecimal getBalanceAlt() {
		return cashAlt != null ? cashAlt : new BigDecimal(0);
	}

	@Override
	public BigDecimal getBalanceMain() {
		return cashMain != null ? cashMain : new BigDecimal(0);
	}

	@Override
	public long getTimestamp() {
		long now = currentTime;
		currentTime = currentTime + this.getIntervallInMillis();

		return now;
	}

	public boolean isFinished() {
		return finished;
	}

	public SimulationStatistic getStats() {
		return stats;
	}

	@Override
	public BigDecimal buyAlt(BigDecimal amountMain, BigDecimal price) {
		if (amountMain.compareTo(cashMain) > 0) {
			throw new IllegalArgumentException("amount may not be greater than cash");
		}
		// calc amount bought
		BigDecimal bought = amountMain.divide(price, MathContext.DECIMAL64);

		// add bought to cash alt
		cashAlt = cashAlt.add(bought);
		// subtract amount main from cash main
		cashMain = cashMain.subtract(amountMain);

		// add new trade
		stats.addTrade(new Trade("0"+this.stats.getTradeCount(),
				this.currentTime, price, bought, "buy"));

		// return amount bought
		return bought;
	}

	@Override
	public BigDecimal sellAlt(BigDecimal amountAlt, BigDecimal price) {
		if (amountAlt.compareTo(cashAlt) > 0) {
			throw new IllegalArgumentException("amount may not be greater than cash");
		}
		// calc amount sold
		BigDecimal sold = amountAlt.multiply(price, MathContext.DECIMAL64);

		// add bought to cash main
		cashMain = cashMain.add(sold);
		// subtract amount alt from cash alt
		cashAlt = cashAlt.subtract(amountAlt);

		// add new trade
		this.stats.addTrade(new Trade("0"+this.stats.getTradeCount(),
				this.currentTime, price, amountAlt, "sell"));

		// return amount sold
		return sold;
	}

	@Override
	public void run() {
		// get market analizer
		MarketAnalyzer analizer = new MarketAnalyzer(this.getPortalGateway(), this.getAnalizeSettings());

		// do as long as end not reached
		boolean buy = true;
		long timestamp = 0;

		while (timestamp < endTime) {
			timestamp = this.getTimestamp();

			// check if buy
			if (buy && analizer.getFutureMoonScore(this.getSymbolPair(), timestamp) >=
					this.getAnalizeSettings().getIntSetting(IntSetting.MOONSOCRE_LIMIT)) {

				// get ask price
				BigDecimal ask = this.getPortalGateway().getTicker(timestamp).getAsk();

				// buy
				this.buyAlt(cashMain, ask);
				buy = false;

			} else if (!buy && analizer.getFutureHellScore(getSymbolPair(), timestamp) <
					this.getAnalizeSettings().getIntSetting(IntSetting.HELLSCORE_LIMIT)){

				// get bid price
				BigDecimal bid = this.getPortalGateway().getTicker(timestamp).getBid();

				// sell
				this.sellAlt(cashAlt, bid);
				buy = true;

			}
		}

		// set finished flag
		this.finished = true;
	}

	@Override
	public int compareTo(SimulationTrader s) {
		if (!this.isFinished() || !s.isFinished()) {
			throw new IllegalThreadStateException("Trading simulation not yet finished");
		}

		// compare stats of simulations
		return this.getStats().compareTo(s.getStats());
	}

	public void printTrades() {
		System.out.println("TRADES ---");
		for (Trade t : this.getStats().getTrades()) {
			System.out.println(t);
		}
		System.out.println("--- TRADES");
	}
}
