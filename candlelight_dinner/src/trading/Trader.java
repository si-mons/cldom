package trading;

import java.math.BigDecimal;

import gateways.AbstractPortalGateway;
import statistic.AnalizeSettings;

public abstract class Trader extends Thread{

	private String symbolAlt;
	private String symbolMain;
	private String symbolPair;
	private String pseudonym;

	private AnalizeSettings analizeSettings;
	private long intervallInMillis;
	private AbstractPortalGateway portalGateway;

	private BigDecimal initialCashMain;
	private BigDecimal initialCashAlt;

	public abstract BigDecimal getBalanceAlt();
	public abstract BigDecimal getBalanceMain();
	public abstract BigDecimal buyAlt(BigDecimal amountMain, BigDecimal price);
	public abstract BigDecimal sellAlt(BigDecimal amountAlt, BigDecimal price);
	public abstract long getTimestamp();

	public Trader(String symbolAlt, String symbolMain, String pseudonym, AnalizeSettings analizeSettings, long intervallInMinutes, AbstractPortalGateway portalGateway) {
		this.symbolAlt = symbolAlt;
		this.symbolMain = symbolMain;
		this.symbolPair = symbolAlt+symbolMain;
		this.pseudonym = pseudonym;

		this.analizeSettings = analizeSettings;
		this.intervallInMillis = intervallInMinutes * 60000;
		this.portalGateway = portalGateway;

		this.initialCashAlt = this.getBalanceAlt();
		this.initialCashMain = this.getBalanceMain();
	}

	public String getSymbolPair() {
		return symbolPair;
	}

	public String getSymbolAlt() {
		return symbolAlt;
	}

	public String getSymbolMain() {
		return symbolMain;
	}

	public String getPseudonym() {
		return pseudonym;
	}

	public AnalizeSettings getAnalizeSettings() {
		return analizeSettings;
	}

	public long getIntervallInMillis() {
		return intervallInMillis;
	}

	public AbstractPortalGateway getPortalGateway() {
		return portalGateway;
	}

	public BigDecimal getInitialCashMain() {
		return initialCashMain;
	}

	public BigDecimal getInitialCashAlt() {
		return initialCashAlt;
	}

	public void setIntervallInMillis(long intervallInMillis) {
		this.intervallInMillis = intervallInMillis;
	}
}
