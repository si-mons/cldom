package statistic;

import gateways.AbstractPortalGateway;
import statistic.AnalizeSettings.BooleanSetting;
import statistic.AnalizeSettings.IntSetting;

import java.math.BigDecimal;
import java.util.List;

public class MarketAnalyzer {

	private ChartPlotter chartPlotter;
	private AnalizeSettings settings;

	public MarketAnalyzer(AbstractPortalGateway portalGateway, AnalizeSettings settings) {
		this.chartPlotter = new ChartPlotter(portalGateway, settings);
		this.settings = settings;
	}

	/**
	 * To determine
	 * @return
	 */
	public int getFutureMoonScore(String symbol, long timestamp) {
		// init score to return
		int moonScore = 0;
		// get the 2 last candles
		Candle prevCandle = chartPlotter.lightACandle(symbol, timestamp, 1);
		Candle currCandle = chartPlotter.lightACandle(symbol, timestamp, 0);

		// the first candle needs at least to be a okay candle
		// but the second one must be reasonably secure
		if (currCandle.getScore() > settings.getIntSetting(IntSetting.MOONSCORE_CURR_CANDLE)
				&& prevCandle.getScore() > settings.getIntSetting(IntSetting.MOONSCORE_PRVS_CANDLE)) {

			// increase moon score
			moonScore = (int)(currCandle.getScore() / 10.0 + prevCandle.getScore() / 10.0);

			// at this point the moving average needs to be compared to the close/open prices
			// get candles for moving average of last candle
			List<Candle> mavgCandlesLast = chartPlotter.lightACandleRange(symbol, timestamp,
					1, settings.getIntSetting(IntSetting.PERIODS_MAVG_BUY));

			// calculate moving average for last candle
			BigDecimal mAVGlastCandle = chartPlotter.calculateSimpleMovingAverage(mavgCandlesLast,
					settings.getBooleanSetting(BooleanSetting.USE_OPEN_PRICES_MAVG_BUY));

			// check if the close price of the last candle is above its moving average
			if (prevCandle.getClose().compareTo(mAVGlastCandle) > 0) {

				// get candles for moving average of current candle
				List<Candle> mAVGcandlesCurrent = chartPlotter.lightACandleRange(symbol, timestamp,
						0, settings.getIntSetting(IntSetting.PERIODS_MAVG_BUY));

				// calculate moving average for current candle
				BigDecimal mAVGcurretCandle = chartPlotter.calculateSimpleMovingAverage(mAVGcandlesCurrent,
						settings.getBooleanSetting(BooleanSetting.USE_OPEN_PRICES_MAVG_BUY));

				// if both open and close prices of the current candle are above its moving average,
				// the price will probably rise at this point
				Candle latestCandle = chartPlotter.lightACandle(symbol, timestamp, 0);
				if (latestCandle.getOpen().compareTo(mAVGcurretCandle) > 0) {
					// increase moon score
					moonScore = moonScore + 10;
				}
				if (latestCandle.getClose().compareTo(mAVGcurretCandle) > 0) {
					// increase moon score
					moonScore = moonScore + 10;
				} else {
					// decrease moon score
					moonScore = moonScore - 20;
				}
			}

			// further checks if there was a price drop before the last two candles
			List<Candle> candlesForPriceDropCheck = chartPlotter.lightACandleRange(symbol, timestamp,
					2, 5);
			for (Candle candle : candlesForPriceDropCheck) {
				// check every candle if its score is negative at least
				if (candle.getScore() > 0) {
					break;
				}
				// increase moon score if candle score is less than -50
				if (candle.getScore() < -50) {
					moonScore = moonScore + 10;
				}
			}
		}

		return moonScore;
	}

	public int getFutureHellScore(String symbol, long timestamp) {
		// get the 2 last candles
		Candle prevCandle = chartPlotter.lightACandle(symbol, timestamp, 1);
		Candle currCandle = chartPlotter.lightACandle(symbol, timestamp, 0);

		// in this case check for a falling price for a pot neg moon score
		if (currCandle.getScore() < settings.getIntSetting(IntSetting.HELLSCORE_CURR_CANDLE)
				&& prevCandle.getScore() < settings.getIntSetting(IntSetting.HELLSCORE_PRVS_CANDLE)) {

			// price is surely falling - return score of worse candle
			int scrPrev = prevCandle.getScore();
			int scrCurr = currCandle.getScore();

			return (int)((scrPrev < scrCurr) ? (scrPrev / 10.0) : (scrCurr / 10.0));
		}

		// return current candle score
		return (int)(currCandle.getScore() / 10.0);
	}

}
