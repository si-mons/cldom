package statistic;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import crypto.Trade;
import gateways.AbstractPortalGateway;
import statistic.AnalizeSettings.BooleanSetting;
import statistic.AnalizeSettings.IntSetting;

public class ChartPlotter {

	private AbstractPortalGateway portalGateway;
	private AnalizeSettings settings;

	public ChartPlotter(AbstractPortalGateway portalGateway, AnalizeSettings settings) {
		this.portalGateway = portalGateway;
		this.settings = settings;
	}

	public Candle lightACandle(String symbol, long timestamp, int index) {
		return settings.getBooleanSetting(BooleanSetting.USE_HEIKIN) ?
				lightAHeikinCandle(symbol, timestamp, index) :
					lightADefaultCandle(symbol, timestamp, index);
	}

	public List<Candle> lightACandleRange(String symbol, long timestamp, int index, int count){
		return settings.getBooleanSetting(BooleanSetting.USE_HEIKIN) ?
				lightAHeikinCandleRange(symbol, timestamp, index, count) :
					lightADefaultCandleRange(symbol, timestamp, index, count);
	}

	public Candle lightADefaultCandle(String symbol, long timestamp, int index) {
		// calculate from and till timestamps from intervall and index provided
		long intervallInMilli = settings.getIntSetting(IntSetting.INTERVALL_MINUTES) * 60 * 1000;
		long till = timestamp - (intervallInMilli * index);
		long from = timestamp - (intervallInMilli * (index + 1));
 
		// get trades for the candle to create
		LinkedHashSet<Trade> trades = portalGateway.getPublicTrades(from, till);

		// create the candle
		Candle candle = new Candle(trades);

		return candle;
	}

	public List<Candle> lightADefaultCandleRange(String symbol, long timestamp, int index, int count) {
		if (count < 1) {
			throw new RuntimeException("'count' may not be null");
		}

		// calculate from and till timestamps from intervall, index and count provided
		long intervallInMilli = settings.getIntSetting(IntSetting.INTERVALL_MINUTES) * 60 * 1000;
		long till = timestamp - (intervallInMilli * index);
		long from = timestamp - (intervallInMilli * (index + count));
		// get all trades needed
		LinkedHashSet<Trade> trades = portalGateway.getPublicTrades(from, till);

		HashMap<Integer, LinkedHashSet<Trade>> groupedTrades = new HashMap<Integer, LinkedHashSet<Trade>>();
		for (int i = 1; i <= count; i++) {
			// calculate timestamp
			long cTill = timestamp - (intervallInMilli * (index + i - 1));
			long cFrom = timestamp - (intervallInMilli * (index + i));

			// check trade dates
			for (Trade trade : trades) {
				if (trade.getDate() >= cFrom && trade.getDate() < cTill) {
					// add to current group
					if (! groupedTrades.containsKey(i)) {
						groupedTrades.put(i, new LinkedHashSet<Trade>());
					}
					groupedTrades.get(i).add(trade);
				}
			}
		}

		// light range of candles from grouped trades
		List<Candle> candleRange = new ArrayList<>(count);
		for (LinkedHashSet<Trade> tradeList : groupedTrades.values()) {
			candleRange.add(new Candle(tradeList));
		}

		return candleRange;
	}

	public HeikinCandle lightAHeikinCandle(String symbol, long timestamp, int index) {
		return this.lightAHeikinCandle(symbol, timestamp, index, settings.getIntSetting(IntSetting.HEIKIN_RECURSION_CNT));
	}
	
	public HeikinCandle lightAHeikinCandle(String symbol, long timestamp, int index, int recursionCount) {
		if (recursionCount < 1) {
			throw new IllegalArgumentException("recursion count may not be less than 1");
		}

		// get previous candle for heikin candle calculation
		Candle previousCandle = null;
		if (recursionCount == 1) {
			previousCandle = lightADefaultCandle(symbol, timestamp, index++);
		} else {
			previousCandle = lightAHeikinCandle(symbol, timestamp, ++index, --recursionCount);
		}
		// calculate from and till timestamps from intervall and index provided
		long intervallInMilli = settings.getIntSetting(IntSetting.INTERVALL_MINUTES) * 60 * 1000;
		long till = timestamp - (intervallInMilli * index);
		long from = timestamp - (intervallInMilli * (index + 1));
 
		// get trades for the candle to create
		LinkedHashSet<Trade> trades = portalGateway.getPublicTrades(from, till);

		// create new heikin candle using previous candle
		HeikinCandle candle = new HeikinCandle(trades, previousCandle);

		// return heikin candle
		return candle;
	}

	public List<Candle> lightAHeikinCandleRange(String symbol, long timestamp, int index, int count) {
		return this.lightAHeikinCandleRange(symbol, timestamp, index, count, settings.getIntSetting(IntSetting.HEIKIN_RECURSION_CNT));
	}

	public List<Candle> lightAHeikinCandleRange(String symbol, long timestamp, int index, int count, int recursionCount) {
		// just light one heikin candle with recursion = choosen recursion plus count
		HeikinCandle c = lightAHeikinCandle(symbol, timestamp, index, recursionCount + count);
		// use all previous candles to fill the list
		List<Candle> candleRange = new ArrayList<>(count);
		HeikinCandle previousCandle = c;

		for (int i = 0; i < count; i++) {

			// add current heikin candle
			candleRange.add(previousCandle);

			// check if previous candle still heikin
			Candle tempPCandle = previousCandle.getPreviousCandle();
			if (tempPCandle.getClass().equals(previousCandle.getClass())) {
				previousCandle = (HeikinCandle) tempPCandle;
			}
		}

		// return candle range
		return candleRange;
	}

	public BigDecimal calculateSimpleMovingAverage(List<Candle> candles, boolean useOpenPrices) {
		// add all trade open/close price
		BigDecimal candlePrice = new BigDecimal(0);
		for (Candle candle : candles) {
			BigDecimal price = useOpenPrices ? candle.getOpen() : candle.getClose();
			candlePrice = candlePrice.add(price);
		}

		// calculate the simple moving average of all candles
		BigDecimal candleSma = candlePrice.divide(new BigDecimal(candles.size()), MathContext.DECIMAL32);

		return candleSma;
	}
}
