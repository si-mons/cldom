package statistic;

import java.util.HashMap;

public class AnalizeSettings {

	private String name;

	private HashMap<IntSetting, Integer> intSettingsMap;
	private HashMap<BooleanSetting, Boolean> booleanSettingsMap;

	public enum IntSetting {
		INTERVALL_MINUTES,
		PERIODS_MAVG_BUY,
		MOONSCORE_CURR_CANDLE,
		MOONSCORE_PRVS_CANDLE,
		MOONSOCRE_LIMIT,
		HELLSCORE_CURR_CANDLE,
		HELLSCORE_PRVS_CANDLE,
		HELLSCORE_LIMIT,
		USE_HEIKIN,
		HEIKIN_RECURSION_CNT;
	};

	public enum BooleanSetting {
		USE_OPEN_PRICES_MAVG_BUY,
		USE_HEIKIN;
	};

	public AnalizeSettings(String name) {
		// set name
		this.name = name;
		// create maps
		this.intSettingsMap = new HashMap<IntSetting, Integer>(IntSetting.values().length);
		this.booleanSettingsMap = new HashMap<BooleanSetting, Boolean>(BooleanSetting.values().length);

		// set default values
		setIntSetting(IntSetting.INTERVALL_MINUTES, 30).
		setIntSetting(IntSetting.PERIODS_MAVG_BUY, 40).

		setIntSetting(IntSetting.MOONSCORE_CURR_CANDLE, 90).
		setIntSetting(IntSetting.MOONSCORE_PRVS_CANDLE, 70).
		setIntSetting(IntSetting.MOONSOCRE_LIMIT, 50).

		setIntSetting(IntSetting.HELLSCORE_CURR_CANDLE, 90).
		setIntSetting(IntSetting.HELLSCORE_PRVS_CANDLE, 70).
		setIntSetting(IntSetting.HELLSCORE_LIMIT, 50).

		setBooleanSetting(BooleanSetting.USE_OPEN_PRICES_MAVG_BUY, false).
		setBooleanSetting(BooleanSetting.USE_HEIKIN, false).
		setIntSetting(IntSetting.HEIKIN_RECURSION_CNT, 1);
	}

	public AnalizeSettings() {
		this("DEFAULT");
	}

	public AnalizeSettings setIntSetting(IntSetting setting, int value) {
		this.intSettingsMap.put(setting, value);

		return this;
	}

	public AnalizeSettings setBooleanSetting(BooleanSetting setting, boolean value) {
		this.booleanSettingsMap.put(setting, value);

		return this;
	}

	public AnalizeSettings setName(String name) {
		this.name = name;

		return this;
	}

	public int getIntSetting(IntSetting setting) {
		return this.intSettingsMap.get(setting);
	}

	public boolean getBooleanSetting(BooleanSetting setting) {
		return this.booleanSettingsMap.get(setting);
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
//		return String.format("[AS-%s-IIM%s-PMAVGB%s-UOPMAVGB%s-MSCCCG%s-MSPCCG%s-FCCI%s-MSLB%s-MSLS%s-%s]", this.getName(), this.getIntervalInMinutes(), 
//				this.getPeriodsMAVGbuy(), this.useOpenPricesMAVGbuy(),
//				this.getMinScoreCurrentCandleConsiderGood(), this.getMinScorePreviousCandleConsiderGood(),
//				this.getFallingCandleCheckIntervall(),
//				this.getMoonScoreLimitBuy(), this.getMoonScoreLimitSell(),
//				this.useHeikinCandles() ? ("HEIKIN" + this.getHeikinRecursionCount()) : "DEFAULT");
		return "todo analize settings toString";
	}

}
