package statistic;

import java.math.BigDecimal;
import java.util.TreeSet;

import gateways.AbstractPortalGateway;
import statistic.AnalizeSettings.BooleanSetting;
import statistic.AnalizeSettings.IntSetting;
import trading.SimulationTrader;
import util.DBConnector;

public class Simulation {

	private long start;
	private long end;

	private AbstractPortalGateway portalGateway;

	private static int[] intervallMinutesVals = new int[] {30, 20, 15};
	private static int[] moonscoreCurrCandleVals = new int[] {90, 80, 70, 60, 50, 40, 30, 20};
	private static int[] moonscorePrvsCandleVals = new int[] {90, 80, 70, 60, 50, 40, 30, 20};
	private static int[] moonscoreLimitVals = new int[] {90, 80, 70, 60, 50, 40, 30, 20};
	
	private static int[] hellscoreCurrCandleVals = new int[] {-70, -60, -50, -40, -30, -20};
	private static int[] hellscorePrvsCandleVals = new int[] {-70, -60, -50, -40, -30, -20};
	private static int[] hellscoreLimitVals = new int[] {-70, -60, -50, -40, -30, -20};
	
	private static int[] periodsMAVGVals = new int[] {40, 30, 20, 15, 10, 5};
	private static boolean[] useOpenPricesMAVGbuyVals = new boolean[] {true, false};

	private TreeSet<SimulationTrader> finishedTraders;

	private DBConnector connector;

	public Simulation(long start, long end, AbstractPortalGateway portalGateway) {
		this.start = start;
		this.end = end;
		this.portalGateway = portalGateway;
		this.finishedTraders = new TreeSet<SimulationTrader>();
		this.connector = new DBConnector();
	}

	public void simulate(String symbolAlt, String symbolMain, BigDecimal initialCashMain) {
		// calc simulation count
		int simulationCount = intervallMinutesVals.length *
				moonscoreCurrCandleVals.length *
				moonscorePrvsCandleVals.length *
				moonscoreLimitVals.length *
				hellscoreCurrCandleVals.length *
				hellscorePrvsCandleVals.length *
				hellscoreLimitVals.length *
				periodsMAVGVals.length *
				useOpenPricesMAVGbuyVals.length;

		System.out.println("STARTING " + simulationCount + " TRADING SIMULATIONS");

		// declare vars
		SimulationTrader trader = null;
		SimulationTrader finishedTrader = null;
		long timestampSimulationStarted = System.currentTimeMillis();
		int tradersFinished = 0;

		// simulate every possible setting
		for (int intervallInMinutes : intervallMinutesVals) {
			for (int moonscoreCurr : moonscoreCurrCandleVals) {
				for (int moonscorePrvs : moonscorePrvsCandleVals) {
					for (int moonscoreLimit : moonscoreLimitVals) {
						for (int hellscoreCurr : hellscoreCurrCandleVals) {
							for (int hellscorePrvs : hellscorePrvsCandleVals) {
								for (int hellscoreLimit : hellscoreLimitVals) {
									for (int periodsMAVGbuy : periodsMAVGVals) {
										for (boolean useOpenPricesMAVGbuy : useOpenPricesMAVGbuyVals) {
											// create setting
											AnalizeSettings settings = new AnalizeSettings().
											setName("SIM"+tradersFinished).
									
											setIntSetting(IntSetting.INTERVALL_MINUTES, intervallInMinutes).
											setIntSetting(IntSetting.PERIODS_MAVG_BUY, periodsMAVGbuy).
		
											setIntSetting(IntSetting.MOONSCORE_CURR_CANDLE, moonscoreCurr).
											setIntSetting(IntSetting.MOONSCORE_PRVS_CANDLE, moonscorePrvs).
											setIntSetting(IntSetting.MOONSOCRE_LIMIT, moonscoreLimit).
		
											setIntSetting(IntSetting.HELLSCORE_CURR_CANDLE, hellscoreCurr).
											setIntSetting(IntSetting.HELLSCORE_PRVS_CANDLE, hellscorePrvs).
											setIntSetting(IntSetting.HELLSCORE_LIMIT, hellscoreLimit).
		
											setBooleanSetting(BooleanSetting.USE_OPEN_PRICES_MAVG_BUY, useOpenPricesMAVGbuy);
				
											if (trader != null) {
												// save finished trader
												finishedTrader = trader;
				
												try {
													// wait for previous trader
													trader.join();
				
													if (++tradersFinished % 1000 == 0) {
														System.out.println(tradersFinished + "/" + simulationCount
																+ " Traders finished");
													}
				
													// add to all traders
													finishedTraders.add(trader);
				
												} catch (InterruptedException e) {
													e.printStackTrace();
												}
											}
				
											// create new trader
											trader = new SimulationTrader(symbolAlt, symbolMain, "TEST",
													settings, intervallInMinutes, portalGateway, initialCashMain, start, end);

											// start new trader
											trader.start();

											// store finished trader
//											if (finishedTrader != null) {
//												connector.writeSimulationTrader(timestampSimulationStarted, tradersFinished,
//														symbolAlt+symbolMain, start, end, finishedTrader);
//											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		System.out.println("Simulation finished and stored in database.");
	}

}
