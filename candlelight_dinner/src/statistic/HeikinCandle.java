package statistic;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.LinkedHashSet;

import crypto.Trade;

public class HeikinCandle extends Candle{

	private Candle previousCandle;

	private BigDecimal haOpen;
	private BigDecimal haClose;
	private BigDecimal haHigh;
	private BigDecimal haLow;

	public HeikinCandle(LinkedHashSet<Trade> trades, Candle previousCandle) {
		super(trades);
		this.previousCandle = previousCandle;
		calculate();
	}

	private void calculate() {
		if (this.getTrades().isEmpty()) {
			this.haOpen = this.haClose = this.haHigh = this.haLow = new BigDecimal("0");
			return;
		}

		// HA-Close = (Open(0) + High(0) + Low(0) + Close(0)) / 4
		this.haClose = this.open.add(this.high).add(this.low).add(this.close)
				.divide(new BigDecimal(4), MathContext.DECIMAL32);

		// HA-Open = (HA-Open(-1) + HA-Close(-1)) / 2
		this.haOpen = previousCandle.getOpen().add(previousCandle.getClose())
				.divide(new BigDecimal(2), MathContext.DECIMAL32);

		// HA-High = Maximum of the High(0), HA-Open(0) or HA-Close(0) 
		if (this.haOpen.compareTo(this.haClose) > 0) {
			this.goingDown = true;
			this.haHigh = this.haOpen.compareTo(this.high) > 0 ? haOpen : high;
		} else {
			this.goingDown = false;
			this.haHigh = this.haClose.compareTo(this.high) > 0 ? haClose : high;
		}
		
		// HA-Low = Minimum of the Low(0), HA-Open(0) or HA-Close(0)
		this.haLow = this.goingDown ? (this.low.compareTo(this.haClose) < 0 ? this.low : this.haClose)
				: (this.low.compareTo(this.haOpen) < 0 ? this.low : this.haClose);
	}

	public Candle getPreviousCandle() {
		return previousCandle;
	}

	@Override
	public BigDecimal getOpen() {
		return this.haOpen;
	}
	
	@Override
	public BigDecimal getHigh() {
		return this.haHigh;
	}
	
	@Override
	public BigDecimal getLow() {
		return this.haLow;
	}

	@Override
	public BigDecimal getClose() {
		return this.haClose;
	}

}
