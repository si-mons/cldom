package statistic;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashSet;
import java.util.LinkedHashSet;

import crypto.Trade;

public class Candle {

	private LinkedHashSet<Trade> trades;
	protected BigDecimal open;
	protected BigDecimal close;
	protected BigDecimal high;
	protected BigDecimal low;
	protected boolean goingDown;

	public Candle(LinkedHashSet<Trade> trades) {
		this.trades = trades;
		calculate();
	}

	/**
	 * Calculates open, close, high and low for own trades
	 */
	private void calculate() {
		if (this.getTrades().isEmpty()) {
			this.open = this.close = this.high = this.low = new BigDecimal("0");
			return;
		}

		Trade openingTrade = null;
		Trade closingTrade = null;
		// determine open/close/high/low trade
		for (Trade trade : trades) {
			// check open
			if (openingTrade == null) {
				openingTrade = trade;
			} else if (trade.compareTo(openingTrade) < 0){
				openingTrade = trade;
			}

			// check close
			if (closingTrade == null) {
				closingTrade = trade;
			} else if (trade.compareTo(closingTrade) > 0){
				closingTrade = trade;
			}

			// check high
			if (this.high == null) {
				this.high = trade.getPrice();
			} else if (trade.getPrice().compareTo(this.high) == 1){
				this.high = trade.getPrice();
			}

			// check low
			if (this.low == null) {
				this.low = trade.getPrice();
			} else if (trade.getPrice().compareTo(this.low) == -1){
				this.low = trade.getPrice();
			}
		}

		// save prices of opening and closing trades
		this.open = openingTrade.getPrice();
		this.close = closingTrade.getPrice();

		// check if going down
		goingDown = this.open.compareTo(this.close) > 0;
	}

	public HashSet<Trade> getTrades() {
		return trades;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public BigDecimal getLow() {
		return low;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public BigDecimal getClose() {
		return close;
	}

	public boolean isGoingDown() {
		return goingDown;
	}

	/**
	 * Calculates a candle score (0-100)
	 * negative score means the candle is falling
	 * positive score means the candle is rising
	 * ---
	 * -100 - -51 means the candle is safely falling (very bad)
	 * -50 - -1 means the candle is insecure but falling (still bad)
	 * ---
	 * 1 - 50 means the candle is insecure but rising (good to hold but not to buy)
	 * 51 - 100 means the candle is strongly rising (good to buy)
	 * 100 = perfect rising candle, the low was the open as well as the high was the close
	 * @return int score of the candle
	 */
	public int getScore() {
		// check if there are trades
		if (this.getTrades().size() == 0) {
			return 0;
		}

		// get body size (diff between open and close)
		BigDecimal bodySize = this.isGoingDown() ? 
				this.getOpen().subtract(this.getClose()) :
					this.getClose().subtract(this.getOpen());
		// get extreme size (diff between high and low)
		BigDecimal extremeSize = this.getHigh().subtract(this.getLow());
		BigDecimal percentage = new BigDecimal(0);
		
		try {
			percentage = bodySize.divide(extremeSize, MathContext.DECIMAL32)
					.multiply(new BigDecimal(100), MathContext.DECIMAL32);
			percentage = percentage.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		} catch (ArithmeticException ae){
			percentage = new BigDecimal(0);
		}

		// if the candle is decreasing, the higher the percentage the worse (due to negation)
		if (this.isGoingDown()) {
			percentage = percentage.negate();
		}

		return percentage.intValue();
	}

	/**
	 * Calculates how much percentage difference is from open to close
	 * @return int percentage difference
	 */
	public int getBodySize() {
		// check if there are trades
		if (this.getTrades().size() == 0) {
			return 0;
		}

		// calculate percentage
		BigDecimal diff = null;
		if (this.isGoingDown()) {
			diff = this.getOpen().divide(this.getClose(),
					MathContext.DECIMAL32);
		} else {
			diff = this.getClose().divide(this.getOpen(),
					MathContext.DECIMAL32);
		}
		// round to body size
		diff = diff.subtract(new BigDecimal(1)).multiply(new BigDecimal(10000));

		return diff.intValue();
	}

	@Override
	public String toString() {
		return String.format("LIGHT FROM THE CANDLE\n> HIGH\t%s\n> OPEN\t%s\n> CLOSE\t%s\n> LOW\t%s\n--  SCORE: %s - SIZE: %s  --",
				this.getHigh(), this.getOpen(), this.getClose(), this.getLow(), this.getScore(), this.getBodySize());
	}
}
