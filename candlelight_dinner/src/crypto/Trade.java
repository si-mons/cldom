package crypto;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Trade implements Comparable<Trade>{

	private String tid;
	private long date;
	private BigDecimal price;
	private BigDecimal amount;
	private String side;

	public Trade(String tid, long date, BigDecimal price, BigDecimal amount) {
		this(tid, date, price, amount, null);
	}

	public Trade(String tid, long date, BigDecimal price, BigDecimal amount, String side) {
		this.tid = tid;
		this.date = date;
		this.price = price;
		this.amount = amount;
		this.side = side;
	}

	public Trade(long date) {
		this.tid = "0";
		this.date = date;
	}

	public String getTid() {
		return tid;
	}

	public long getDate() {
		return date;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getSide() {
		return side;
	}

	@Override
	/**
	 * returns 1 if this happend later (date bigger than t)
	 * returns -1 if this happend earlier (date smaller than t)
	 */
	public int compareTo(Trade t) {
		// calc diff between dates
		long diff = this.getDate() - t.getDate();
		if (diff == 0) {
			// check tid to see wich one happend earier (rare case but can happen)
			diff = new Long(this.getTid()) - new Long(t.getTid());
			
		}
		return diff == 0 ? 0 :(diff > 0 ? 1 : -1);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass().equals(this.getClass())) {
			Trade t = (Trade) obj;
			return this.getTid().equals(t.getTid());
		}

		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return new Integer(this.tid).hashCode();
	}

	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy hh:mm");
		return String.format("[T%s%s\t%s\tAT\t%s;\t%s]", this.getTid(), this.getSide() != null ? "\t"+this.getSide():"", this.getAmount(), this.getPrice(), df.format(this.getDate()));
	}

}
