package crypto;

import java.math.BigDecimal;

public class Ticker {

	private String symbol;
	private BigDecimal bid;
	private BigDecimal ask;

	public Ticker(String symbol, BigDecimal bid, BigDecimal ask) {
		this.symbol = symbol;
		this.bid = bid;
		this.ask = ask;
	}

	public String getSymbol() {
		return symbol;
	}

	public BigDecimal getBid() {
		return bid;
	}

	public BigDecimal getAsk() {
		return ask;
	}

	@Override
	public String toString() {
		return String.format("[TICKER BID: %s ASK: %s]", this.getBid(), this.getAsk());
	}
}
