package util;

import java.util.LinkedHashSet;
import java.util.LinkedList;

import crypto.Trade;
import gateways.AbstractPortalGateway;
import gateways.HitBtcGateway;
import util.DBUtil.WriterThread;

public class ImportUtil {

	public static void importOneYearTestTrades(DBConnector connector) {
		// end time
		long end = System.currentTimeMillis();
		// one day
		long oneDay = 86400000l;
		// array of symbols to add
		String[] symbols = new String[] {"EOSBTC", "DASHBTC", "ETHBTC", "STRATBTC", "ZECBTC", "LTCBTC"};
		// add one year of all symbols
		for (int i = 0; i < symbols.length; i++) {
			System.out.println("imporing "+symbols[i]+"..");
			HitBtcGateway portalGateway = new HitBtcGateway(symbols[i]);
			// track time
			long t = System.currentTimeMillis();
			int cnt = importTrades(symbols[i], end - oneDay * 50l, end, portalGateway, connector);

			System.out.println(cnt + " " + symbols[i] + " TRADES IMPORTED! "+(System.currentTimeMillis()-t)+"ms");
		}
	}

	public static int importTrades(String symbol, long from, long till, AbstractPortalGateway portalGateway, DBConnector connector) {
		// result count
		int count = 0;
		// split read write in 2 day chunks (good fetch range/packet size)
		long timeRangeEach = 172800000;

		// create threads
		LinkedList<WriterThread> writerThreads = new LinkedList<WriterThread>();
		for (long thrFrom = from; thrFrom < till; thrFrom = thrFrom+timeRangeEach) {
			// calculate from and till of thread
			long thrTill = (thrFrom + timeRangeEach >= till) ? till : thrFrom + timeRangeEach - 1;
			// read trades
			LinkedHashSet<Trade> trades = portalGateway.getPublicTrades(thrFrom, thrTill, true);

			// increase count
			count = count + trades.size();

			// execute write inside thread
			WriterThread thread = new WriterThread(connector, symbol, trades);
			thread.start();

			writerThreads.add(thread);
		}

		// wait for the threads to complete
		for (WriterThread t : writerThreads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return count;
	}
}
