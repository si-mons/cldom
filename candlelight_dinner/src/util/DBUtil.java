package util;

import java.util.LinkedHashSet;
import java.util.LinkedList;

import crypto.Trade;

public class DBUtil {

	public static class WriterThread extends Thread {

		private DBConnector connector;
		private String symbol;
		private LinkedHashSet<Trade> trades;

		public WriterThread(DBConnector connector, String symbol, LinkedHashSet<Trade> trades) {
			this.connector = connector;
			this.symbol = symbol;
			this.trades = trades;
		}

		@Override
		public void run() {
			connector.writeTrades(symbol, trades);
		}

	}

	public static class ReaderThread extends Thread {

		private DBConnector connector;
		private String symbol;
		private long from;
		private long till;
		private LinkedHashSet<Trade> trades;

		public ReaderThread(DBConnector connector, String symbol, long from, long till) {
			this.connector = connector;
			this.symbol = symbol;
			this.from = from;
			this.till = till;
			this.trades = new LinkedHashSet<Trade>();
		}

		@Override
		public void run() {
			trades = connector.readTrades(symbol, from, till);
		}

		public LinkedHashSet<Trade> getTrades() {
			if (this.isAlive()) {
				throw new IllegalAccessError("thread still alive");
			}

			return this.trades;
		}
	}

	public static LinkedHashSet<Trade> loadTrades(String symbol, long from, long till, DBConnector connector) {
		// create a thread for every day (but not gt 35/lt 3 threads total)
		int twlfHrs = 43200000;
		// make threads equal in size
		int threadCount = (int)((till-from) / twlfHrs);
		threadCount = threadCount > 35 ? 35 : threadCount < 3 ? 3 : threadCount;

		// get equal time range to use for every thread based on thread count
		long timeRangeEach = (till-from) / threadCount;

		// create threads
		LinkedList<ReaderThread> readerThreads = new LinkedList<ReaderThread>();
		for (int i = 0; i < threadCount; i++) {
			// calculate from and till of thread
			long thrFrom = from + (timeRangeEach * i);
			long thrTill = (i == threadCount-1) ? till : thrFrom + timeRangeEach - 1;
			// create and start thread
			ReaderThread thread = new ReaderThread(connector, symbol, thrFrom, thrTill);
			thread.start();

			// add thread to list
			readerThreads.add(thread);
		}

		// create result set
		LinkedHashSet<Trade> trades = new LinkedHashSet<Trade>();

		// wait for the threads to complete
		for (ReaderThread t : readerThreads) {
			try {
				t.join();
				// add trades from thread
				trades.addAll(t.getTrades());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return trades;
	}

	public static Trade loadTrade(String symbol, long from, String side, DBConnector connector) {
		// get next trade
		LinkedHashSet<Trade> trades = connector.readTrades(symbol, from, from + 604800000l, 1, side);
		Trade trade = null;
		for (Trade t : trades) {
			trade = t;
			break;
		}

		// return trade
		return trade;
	}
}
