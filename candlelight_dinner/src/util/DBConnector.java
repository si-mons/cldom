package util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;

import crypto.Trade;
import statistic.AnalizeSettings.BooleanSetting;
import statistic.AnalizeSettings.IntSetting;
import trading.SimulationTrader;

public class DBConnector {

	private String uid = "root";
	private String pwd = "";
	private String db_name = "cldom";

	private Connection db;

	private class CountingOutputStream extends OutputStream {
		private int total;

		public int getTotal() {
			return total;
		}

		@Override
		public void write(int i) {
			throw new RuntimeException("not implemented");
		}

		@Override
		public void write(byte[] b) {
			total += b.length;
		}

		@Override
		public void write(byte[] b, int offset, int len) {
			total += len;
		}
	}
	
	public DBConnector() {
		String url = "jdbc:mysql://localhost/" + db_name;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			db = DriverManager.getConnection(url, uid, pwd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void writeSimulationTrader(long timestamp, int idx, String symbol, long startTime, long endTime, SimulationTrader trader) {
		// create date formatter 2017-10-01 13:09:13
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		// create query string
		String queryString = "INSERT INTO simulation (timestamp, idx, symbol, start, end, performance_pct, trades_profit, trades_even, trades_loss, "
				+ "interval_minutes, periods_mavg_buy, use_open_price_mavg_buy, moonscore_curr_candle, moonscore_prvs_candle, "
				+ "moonscore_limit, hellscore_curr_candle, hellscore_prvs_candle, hellscore_limit, use_heikin, heikin_recursion_cnt) "
				+ "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
		// fill query string with values
		queryString = String.format(queryString, timestamp, idx, symbol, sdf.format(new Date(startTime)), sdf.format(new Date(endTime)),
				trader.getStats().getPerformancePercentage(), trader.getStats().getCountProfitSells(), trader.getStats().getCountEvenSells(),
				trader.getStats().getCountLosingSells(), trader.getAnalizeSettings().getIntSetting(IntSetting.INTERVALL_MINUTES),
				trader.getAnalizeSettings().getIntSetting(IntSetting.PERIODS_MAVG_BUY),
				trader.getAnalizeSettings().getBooleanSetting(BooleanSetting.USE_OPEN_PRICES_MAVG_BUY) ? "1" : "0",
				trader.getAnalizeSettings().getIntSetting(IntSetting.MOONSCORE_CURR_CANDLE),
				trader.getAnalizeSettings().getIntSetting(IntSetting.MOONSCORE_PRVS_CANDLE),
				trader.getAnalizeSettings().getIntSetting(IntSetting.MOONSOCRE_LIMIT),
				trader.getAnalizeSettings().getIntSetting(IntSetting.HELLSCORE_CURR_CANDLE),
				trader.getAnalizeSettings().getIntSetting(IntSetting.HELLSCORE_PRVS_CANDLE),
				trader.getAnalizeSettings().getIntSetting(IntSetting.HELLSCORE_LIMIT),
				trader.getAnalizeSettings().getBooleanSetting(BooleanSetting.USE_HEIKIN) ? "1" : "0",
				trader.getAnalizeSettings().getIntSetting(IntSetting.HEIKIN_RECURSION_CNT));
		// try to execute query
		try {
			Statement s = db.createStatement();
			s.execute(queryString.toString());
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void writeTrades(String symbol, LinkedHashSet<Trade> trades) {
		// create initial query string
		StringBuilder queryString = null;
		try {
			boolean first = true;
			String prefix = "('";
			String splitter = "','";
			String postFix = "')";
			for (Trade trade : trades) {
				// create value string
				if (!first) {
					queryString.append(",");
				} else {
					queryString = new StringBuilder("INSERT IGNORE INTO trades (tid, symbol, date, price, amount, side) VALUES ");
					first = false;
				}

				queryString.append(prefix);
				queryString.append(trade.getTid());
				queryString.append(splitter);
				queryString.append(symbol);
				queryString.append(splitter);
				queryString.append(trade.getDate());
				queryString.append(splitter);
				queryString.append(trade.getPrice());
				queryString.append(splitter);
				queryString.append(trade.getAmount());
				queryString.append(splitter);
				queryString.append(trade.getSide());
				queryString.append(postFix);

				// check string size
				if (getBytes(queryString.toString()) > 1000000) {
					// write to db before string becomes too big and continue
					Statement s = db.createStatement();
					s.execute(queryString.toString());
					s.close();
					// and continue
					first = true;
				}
			}

			// write remaining trades
			if (!first) {
				Statement s = db.createStatement();
				s.execute(queryString.toString());
				s.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteTrades(String symbol, long from, long till) {
		try {
			// read
			Statement s = db.createStatement();
			String q = String.format("DELETE FROM trades WHERE date >= %s AND date < %s AND symbol LIKE '%s' ORDER BY date desc;",
					from, till, symbol);
			// execute query
			s.execute(q);
			s.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public LinkedHashSet<Trade> readTrades(String symbol, long from, long till) {
		return readTrades(symbol, from, till, 0);
	}

	public LinkedHashSet<Trade> readTrades(String symbol, long from, long till, int maxResults) {
		return readTrades(symbol, from, till, maxResults, null);
	}

	public LinkedHashSet<Trade> readTrades(String symbol, long from, long till, int maxResults, String side) {
		try {
				// side restriction check
				String sideRestriction = "";
				if (side != null && (side.equals("sell") || side.equals("buy"))) {
					sideRestriction = String.format(" and side LIKE '%s' ", side);
				}

				// read
				Statement s = db.createStatement();
				String q = String.format("SELECT * FROM trades WHERE date >= %s "
						+ "AND date < %s AND symbol LIKE '%s' %s ORDER BY date asc %s;",
						from, till, symbol, sideRestriction, maxResults > 0 ? "LIMIT " + maxResults : "");

				// execute query
				ResultSet r = s.executeQuery(q);
				// move every row into an trade obj of an list
				LinkedHashSet<Trade> trades = new LinkedHashSet<Trade>();

				while(r.next()) {
					trades.add(new Trade(r.getString(1), r.getLong(3),
							r.getBigDecimal(4), r.getBigDecimal(5), r.getString(6)));
				}

				r.close();
				s.close();

				// return trades
				return trades;

		} catch (Exception e) {
			e.printStackTrace();

			// return empty list
			return new LinkedHashSet<Trade>();
		}
	}

	private int getBytes(String s) {
		// get counting outputstream
		CountingOutputStream cos = new CountingOutputStream();
		Writer writer;

		try {
			writer = new OutputStreamWriter(cos, "UTF-8");
			for (int i = 0; i < s.length(); i += 8096) {
				int end = Math.min(s.length(), i + 8096);
				writer.write(s, i, end - i);
			}
			writer.flush();
			writer.close();

			// return size
			return cos.getTotal();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return -1;
	}
}
